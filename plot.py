import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sb
from sklearn.metrics import classification_report, confusion_matrix, plot_confusion_matrix


plot_fig_count = 1

class Plots():

    def create_confusion_matrix(class_act,class_pred,title,save_path):
        global plot_fig_count
        plt.clf()
        plt.figure(plot_fig_count)

        cm = confusion_matrix(class_act, class_pred)
        group_names = ['True Neg','False Pos','False Neg','True Pos']

        group_counts = ['{0:0.0f}'.format(value) for value in
                        cm.flatten()]

        group_percentages = ['{0:.2%}'.format(value) for value in
                             cm.flatten()/np.sum(cm)]

        labels = [f'{v1}\n{v2}\n{v3}' for v1, v2, v3 in
                  zip(group_names,group_counts,group_percentages)]

        labels = np.asarray(labels).reshape(2,2)

        mat = sb.heatmap(cm, annot=labels, fmt='', cmap='Blues')

        figure = mat.get_figure()
        plt.title('Confusion_Matrix ' + title)
        plt.xlabel('Predicted')
        plt.ylabel('True')
        figure.savefig(save_path+'/'+title+'_Confusion_Matrix.png', dpi=400)

        plot_fig_count += 1

    def pdf_curve(vals,pdf):
        global plot_fig_count
        plt.clf()
        plt.figure(plot_fig_count)
        sb.set_style('whitegrid')
        sb.lineplot(x=vals, y=pdf , color = 'black')
        plt.xlabel('Heights')
        plt.ylabel('Probability Density')
        plt.savefig('plots/pdf_curve.png')

        plot_fig_count += 1

    def createPerformancePlot(history,month,year,sensor_id,duration,save_path):
        global plot_fig_count
        plt.clf()
        # plot train and validation loss
        plt.figure(plot_fig_count)
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model train vs validation loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper right')
        plt.savefig(save_path+'/PERFORMANCE.png', dpi=600)
        plot_fig_count += 1

    def createWindowPlot(type,window_df,args,faulty,save_path):
        if faulty == 0:
            faulty = False
        else:
            faulty = True

        global plot_fig_count
        plt.clf()
        fig = plt.figure(plot_fig_count)
        plt.title('Window Error '+type)
        plt.plot(window_df, linewidth=1.0,label='Window % Classified as Error')
        plt.axhline(args.error_threshold, linewidth=1.0,color='darkviolet',label='Error Threshold')
        plt.legend(loc='upper left')
        plt.ylabel('%')
        plt.xlabel('Time')
        ax = fig.add_subplot()
        ax.grid(False)
        ax.text(0.7, 0.85, 'Window Length:' +  str(30), style='italic',transform=ax.transAxes)
        ax.text(0.7, 0.75, 'Classified as Faulty?'+  str(faulty), style='italic',transform=ax.transAxes)

        plt.savefig(save_path+'/Window_Classifier.png', dpi=600)

        plot_fig_count += 1

    def createProbabilityPlot(args,type,df,save_path):
        plt.clf()
        global plot_fig_count
        fig = plt.figure(plot_fig_count)
        plt.title('CDF Probability '+type)
        plt.axhline(args.cdf_threshold*100, linewidth=1.0,color='darkviolet',label='Error Threshold')
        plt.plot(100*df, linewidth=1.0,linestyle='--',label='Absolute Difference Probability')
        plt.legend(loc='upper left')
        plt.ylabel('%')
        plt.xlabel('Time')
        ax = fig.add_subplot()
        ax.grid(False)
        plt.savefig(save_path+'/CDF_Probability.png', dpi=600)
        plot_fig_count += 1

    def createActualPlot(month,year,df,sensor,duration,save_path):
        global plot_fig_count
        plt.clf()
        plt.figure(plot_fig_count)
        plot = df.plot.scatter(x='datetime',y='value',c="time",colormap='viridis')
        plt.xticks(np.arange(0,len(df.datetime),step=145),rotation='vertical')
        plt.subplots_adjust(bottom=0.25)

        fig = plot.get_figure()
        fig.set_size_inches((12,12)) # defaults to 8 by 6
        fig.set_dpi(120)
        fig.savefig(save_path+'/ACTUALS.png')
        plot_fig_count += 1

    def createPredActualPlot(type,actualPlot,
                            trainPredictPlot,testPredictPlot,
                            save_path,testScore,trainScore):
        plt.clf()
        global plot_fig_count
        fig = plt.figure(plot_fig_count)

        plt.title('Actual & Predicted '+type)
        plt.plot(actualPlot, linewidth=1.0,color='darkviolet',linestyle='--',label='Actual')
        plt.plot(trainPredictPlot, linewidth=1.0,linestyle='--',label='Train')
        plt.plot(testPredictPlot, linewidth=1.0,color='yellow',linestyle='--',label='Test')
        plt.legend(loc='upper left')
        plt.ylabel('Temp')
        plt.xlabel('Time')
        ax = fig.add_subplot()
        ax.text(0.5, 0.95, 'Test Score: %.2f RMSE' % (testScore), style='italic',transform=ax.transAxes)
        ax.text(0.5, 0.9, 'Train Score: %.2f RMSE' % (trainScore), style='italic',transform=ax.transAxes)
        #added code to save plot to file

        plt.savefig(save_path+'/MODELED.png', dpi=600)

        plot_fig_count += 1
