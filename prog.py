import argparse

class CommandLine():
    def args():
        print('run -h in command line for required arg detail')

        parser = argparse.ArgumentParser(description='Process some integers.')

        parser.add_argument('--month',
                            type=int,
                            help='month to start parse from')

        parser.add_argument('--year',
                            help='year to start parse from')

        # parser.add_argument('--sensor_id',
        #                     help='id of sensor to retrieve readings for')

        parser.add_argument('--duration',
                            type=int, default=1, nargs='?',
                            help='number of months to parse, default is 1 if not provided')

        parser.add_argument('--epoch_size',
                            type=int, default=1, nargs='?',
                            help='epoch iterations, default is 1 if not provided')

        parser.add_argument('--batch_size',
                            type=int, default=1, nargs='?',
                            help='model batch size, default is 1 if not provided')

        parser.add_argument('--training_split',
                            type=float, default=0.67, nargs='?',
                            help='model training split, default is 0.67 if not provided')

        parser.add_argument('--cdf_threshold',
                            type=float, default=0.10, nargs='?',
                            help='% threshold for pdf classifier')

        parser.add_argument('--lstm_patience',
                            type=int, default=2, nargs='?',
                            help='patience to wait in model')

        parser.add_argument('--error_threshold',
                            type=int, default=80, nargs='?',
                            help='classifier for deciding if faulty sequence')

        args = parser.parse_args()
        print(vars(args))

        return args
