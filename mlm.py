#!/usr/bin/env python3
import urllib.request, json, csv
import datetime
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
from process import Process as p
from process import Utils
from plot import Plots

# lstm imports
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, ModelCheckpoint

from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence

# code in this file has been taken from machinelearningmastery.com, code is unalterered
# unless stated. adaptations will be highlighted before the respective function

class MachineLearningMastery():

# changes:
    # return statement to allow returning of data types.
    # plot save functionality

    # def svm(dataframe):


    def lstm_classifier(dataframe):
        # LSTM for sequence classification in the IMDB dataset
        # fix random seed for reproducibility
        np.random.seed(7)
        # load the dataset but only keep the top n words, zero the rest
        # truncate and pad input sequences
        # X = dataframe['Abs_diff']
        X = dataframe['cdf']
        y = dataframe['class']

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)
        print(X_train.shape,X_test.shape,y_train.shape,y_test.shape)

        X_train = X_train.values.reshape(-1, 1, 1)
        X_test  = X_test.values.reshape(-1, 1, 1)
        y_train = y_train.values.reshape(-1, 1, 1)
        y_test = y_test.values.reshape(-1, 1, 1)

        model = Sequential()
        model.add(LSTM(100, return_sequences=True))
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        model.fit(X_train, y_train, epochs=3, batch_size=1)
        print('model', model)
        # Final evaluation of the model
        scores = model.evaluate(X_test, y_test, verbose=0)

        # trainPredict = model.predict_classes(X_train)
        # testPredict = model.predict_classes(X_test)
        trainPredict = (model.predict(X_train) > 0.5).astype("int32")
        testPredict = (model.predict(X_test) > 0.5).astype("int32")

        print(trainPredict)
        for i in testPredict:
            print(i)

        print('scores', scores)
        print("Accuracy: %.2f%%" % (scores[1]*100))

    def lstm(dataframe,month,year,
                sensor_id,epoch_size,
                batch_length,duration,training_split,
                lstm_patience,type,save_path,iteration):
        # fix random seed for reproducibility
        np.random.seed(7)

        # load the dataset
        data = dataframe.values
        data = data.astype('float32')

        # normalize the dataset
        scaler = MinMaxScaler(feature_range=(0, 1))
        dataset = scaler.fit_transform(data)

        # split into train and test sets
        train_size = int(len(dataset) * training_split)
        test_size = len(dataset) - train_size
        train, test = dataset[0:train_size,:], dataset[train_size:len(dataset),:]
        print('trainingSamples: ',len(train),'testSamples: ', len(test))

        # reshape into X=t and Y=t+1
        look_back = 1
        trainX, trainY = MachineLearningMastery.create_dataset(train, look_back)
        testX, testY = MachineLearningMastery.create_dataset(test, look_back)

        # reshape input to be [samples, time steps, features]
        trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
        testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
        # only train model on new sensor_id, first iteration
        # if iteration == 0:
            # create and fit the LSTM network
        model = Sequential()
        model.add(LSTM(4, input_shape=(1, look_back)))
        model.add(Dense(1))
        model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mean_squared_error'])

        # train best model
        mc = ModelCheckpoint('models/best_model.h5', monitor='mean_squared_error', save_best_only=True, verbose=1, mode='min')
        es = EarlyStopping(monitor='mean_squared_error', patience=lstm_patience, verbose=1)

        # Stochastic Gradient Descent. Batch Size = 1
        # The number of epochs is a hyperparameter that defines the number
        # times that the learning algorithm will work through the entire training dataset.
        history = model.fit(trainX,
                            trainY,
                            callbacks=[es, mc],
                            epochs=epoch_size,
                            batch_size=batch_length,
                            verbose=2,
                            validation_split=0.33)
        # history keys: ['accuracy', 'loss', 'val_accuracy', 'val_loss']
        Plots.createPerformancePlot(history,month,year,sensor_id,duration,save_path)

        # load the saved model
        saved_model = load_model('models/best_model.h5')
        # make predictions
        trainPredict = saved_model.predict(trainX)
        testPredict = saved_model.predict(testX)
        # invert predictions
        trainPredict = scaler.inverse_transform(trainPredict)
        trainY = scaler.inverse_transform([trainY])

        testPredict = scaler.inverse_transform(testPredict)
        testY = scaler.inverse_transform([testY])
        # calculate root mean squared error
        trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredict[:,0]))
        print('Train Score: %.2f RMSE' % (trainScore))
        testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:,0]))
        print('Test Score: %.2f RMSE' % (testScore))

        # shift train predictions for plotting
        trainPredictPlot = np.empty_like(dataset)
        trainPredictPlot[:, :] = np.nan
        trainPredictPlot[look_back:len(trainPredict)+look_back, :] = trainPredict
        # shift test predictions for plotting
        testPredictPlot = np.empty_like(dataset)
        testPredictPlot[:, :] = np.nan
        testPredictPlot[len(trainPredict)+(look_back*2)+1:len(dataset)-1, :] = testPredict

        # plot baseline and predictions
        actualPlot = scaler.inverse_transform(dataset)

        Plots.createPredActualPlot(type,actualPlot,
                                    trainPredictPlot,
                                    testPredictPlot,
                                    save_path,
                                    testScore,
                                    trainScore)


        return scaler.inverse_transform(test),testPredict,trainPredict, len(train), testScore




# unaltered code from
# https://machinelearningmastery.com/convert-time-series-supervised-learning-problem-python/
# multivar = pd.concat([df37s1,df37s2], axis=1)
# multsdf = mlm.series_to_supervised(multivar,1,1)
# takes in values, then num past sequence, then num future sequence
# sdf = mlm.series_to_supervised(df37s1,1,1)
    def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    	"""
    	Frame a time series as a supervised learning dataset.
    	Arguments:
    		data: Sequence of observations as a list or NumPy array.
    		n_in: Number of lag observations as input (X).
    		n_out: Number of observations as output (y).
    		dropnan: Boolean whether or not to drop rows with NaN values.
    	Returns:
    		Pandas DataFrame of series framed for supervised learning.
    	"""
    	n_vars = 1 if type(data) is list else data.shape[1]
    	df = pd.DataFrame(data)
    	cols, names = list(), list()
    	# input sequence (t-n, ... t-1)
    	for i in range(n_in, 0, -1):
    		cols.append(df.shift(i))
    		names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    	# forecast sequence (t, t+1, ... t+n)
    	for i in range(0, n_out):
    		cols.append(df.shift(-i))
    		if i == 0:
    			names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
    		else:
    			names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
    	# put it all together
    	agg = pd.concat(cols, axis=1)
    	agg.columns = names
    	# drop rows with NaN values
    	if dropnan:
    		agg.dropna(inplace=True)
    	return aggs

    def reshapeSamples(data,length):
        samples = list()

        # split data into samples
        for i in range(0,len(data),length):
            sample = data[i:i+length]
            samples.append(sample)

        # delete last sample in list to ensure all same size
        del samples[-1]

        #reshape data into array of [samples, timesteps, features]
        data = np.array(samples,dtype=object)
        print(data.shape)

        return data

    # convert an array of values into a dataset matrix
    def create_dataset(dataset, look_back=1):
    	dataX, dataY = [], []
    	for i in range(len(dataset)-look_back-1):
    		a = dataset[i:(i+look_back), 0]
    		dataX.append(a)
    		dataY.append(dataset[i + look_back, 0])
    	return np.array(dataX), np.array(dataY)
