#!/usr/bin/env python3
import urllib.request, json, csv
import datetime
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm
import math, sys
import argparse
import seaborn as sb
import os
from random import seed
from random import random
from plot import Plots

# lstm imports
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix, plot_confusion_matrix
from sklearn import svm
from itertools import islice

class Process():

    # def createPredictedCSV(df,file_prepend,iteration):
    #     if iteration == 0:
    #         path = os.getcwd() + '/csv'
    #         Process.create_dir(path,file_prepend)

    def create_dir(path,name):
        try:
            os.mkdir(path+'/'+name)
        except OSError:
            print ("Creation of the directory %s failed" % path)
            print('')
        else:
            print ("Successfully created the directory %s " % path)
            print('')

    def no_fault(sensorReadingsData,split):
        class_as_faulty = 0

        return sensorReadingsData, '', class_as_faulty

    def erratic_failure(sensorReadingsData,split,max=40, min=0,length=40):
        class_as_faulty = 1

        seed(1)
        # generate random numbers between 0-1
        for _ in range(length):
            value = random()
            scaled_value = min + (value * (max - min))

            sensorReadingsData = sensorReadingsData.append({
            'datetime': '2020-05-08 00:00:00',
            'time': 0,
            'value': (round(scaled_value,2)),
            'class':1
            }, ignore_index=True)

        return sensorReadingsData, 'erratic_failure', class_as_faulty

    def temporary_failure(sensorReadingsData,split,length=40,increment=0.25):
        class_as_faulty = 0

        train_size = int(len(sensorReadingsData) * split)
        test_size = len(sensorReadingsData) - train_size

        insert_at = int(train_size + (test_size/2))
        insert_end = int(insert_at + length)
        count = 0
        increase_increment = increment
        decrease_increment = increment
        # every 100 count, increase reading by count +1

        for index, row in sensorReadingsData[insert_at:insert_end].iterrows():
            if (count <= round(length/2)):
                sensorReadingsData.at[index,'value'] = row['value'] + increase_increment
                sensorReadingsData.at[index,'class'] = 1
                increase_increment = increase_increment + increment
                count += 1
            else:
                increase_increment = increase_increment - increment
                sensorReadingsData.at[index,'value'] = row['value'] + increase_increment
                sensorReadingsData.at[index,'class'] = 1
                count += 1

        return sensorReadingsData, 'temporary_failure', class_as_faulty

    def catastrophic_failure(sensorReadingsData,split,length=40, val=40):
        class_as_faulty = 1

        for i in range(length):
            sensorReadingsData = sensorReadingsData.append({
            'datetime': '2020-05-08 00:00:00',
            'time': 0,
            'value': (val),
            'class':1
            }, ignore_index=True)
        return sensorReadingsData, 'catastrophic_failure', class_as_faulty

    def drifting_failure(sensorReadingsData,split):

        class_as_faulty = 1

        train_size = int(len(sensorReadingsData) * split)
        count = 0
        increment = 1
        # every 100 count, increase reading by count +1
        for index, row in sensorReadingsData[train_size:].iterrows():
            if (index%100):
                sensorReadingsData.at[index,'value'] = row['value'] + increment
                sensorReadingsData.at[index,'class'] = 1
            else:
                sensorReadingsData.at[index,'value'] = row['value'] + increment
                sensorReadingsData.at[index,'class'] = 1
                increment += 1

        return sensorReadingsData, 'drifting_failure', class_as_faulty

    def getDataPoints():
        with urllib.request.urlopen("http://smartbms01.shef.ac.uk/sensor-list") as url:
            data = json.loads(url.read().decode())

        return data

    def buildDataSet(sensor_id,month,year,duration):
        Process.createCSV(sensor_id,str(month),year,str(duration))
        monthList = []

        for i in range(0,duration):
            parsedList = Process.parseMonth(sensor_id,str(month+i),year)
            monthList = monthList + parsedList


        fileName = Process.addDictToCSV(sensor_id,monthList,str(month),year,str(duration))
        return fileName, sensor_id

    def getSensorData(sensor_id,start_obj):

        end_obj = start_obj + datetime.timedelta(days=+3,hours=+23,minutes=+59,seconds=+59)

        while end_obj.month != start_obj.month:
            end_obj = end_obj + datetime.timedelta(days=-1)

        url_date = "http://smartbms01.shef.ac.uk/sensor?id={}&start={}T{}&end={}T{}".format(sensor_id,
                        start_obj.date(),start_obj.time(),end_obj.date(),end_obj.time())


        with urllib.request.urlopen(url_date) as url:
            data = json.loads(url.read().decode())

        return data, end_obj

    def parseMonth(id,month,year):

        start = datetime.datetime.strptime("01"+month+year+" 000000", '%d%m%y %H%M%S')
        monthList = []
        convertedMonth = Utils.convertMonth(month)

        while start.strftime("%B") == convertedMonth.strftime("%B"):
            sensorDate = Process.getSensorData(id,start)

            monthList.extend(sensorDate[0]["values"])
            start = sensorDate[1] + datetime.timedelta(days=+1,hours=-23,minutes=-59,seconds=-59)
        return monthList

    def addDictToCSV(name,jsonData,month,year,duration):
        monthName = Utils.convertMonth(month)

        fileName = 'csv/'+name+'-'+monthName.strftime("%B")+year+'-'+duration+'months.csv'
        # list = jsonData["values"]
        list = jsonData
        fieldnames = ['datetime','time','value','class']

        with open(fileName, 'a+', newline='') as csv_file:
        # with open(fileName, mode='w') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            for i in list:
                parsedDate = datetime.datetime.strptime(i["datetime"], '%Y-%m-%dT%H:%M:%SZ')
                writer.writerow({'datetime': parsedDate, 'time': parsedDate.strftime("%H%M"), 'value': i["value"], 'class': 0})
        return fileName

    def createCSV(name,month,year,duration):
        monthName = Utils.convertMonth(month)

        fileName = 'csv/'+name+'-'+monthName.strftime("%B")+year+'-'+duration+'months.csv'

        with open(fileName, mode='w') as csv_file:
            fieldnames = ['datetime','time','value','class']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            writer.writeheader()

    def mean_sd_pdf(x):
        # x.sort()
        #Calculate mean and Standard deviation.
        mean = np.mean(x)
        print('Mean:',mean)
        sd = np.std(x)
        print('SD:',sd)

        pdf = []
        for i in x:
            pdf.append(norm.pdf(i , loc = mean , scale = sd))

        return pdf, mean, sd

    def modeled_dataframe(act,testPred, trainPred, trainLen,sensorReadingsData):
        datTestPred = pd.DataFrame(testPred)
        datTrainPred = pd.DataFrame(trainPred)

        predictions = datTrainPred.append(datTestPred,ignore_index=True)

        predictions.columns = ['Predicted']
        # datTrainPred.columns = ['Predicted']

        sensorReadingsData.drop(0, inplace=True)
        sensorReadingsData.drop(trainLen, inplace=True)
        sensorReadingsData.drop(trainLen-1, inplace=True)

        sensorReadingsData.reset_index(inplace=True)

        df = pd.concat([sensorReadingsData,predictions], axis=1)

        nan_value = float("NaN")
        df.replace("", nan_value, inplace=True)
        df.dropna(subset = ["Predicted"], inplace=True)
        df.drop(columns='index', inplace=True)

        act = df['value'].astype('float32')
        pred = df['Predicted']
        # getting Difference
        df['Abs_diff'] = abs(df['value'].astype('float32') - df['Predicted'])

        # df['MAPE'] = np.abs((act - pred) / pred) * 100

        return df

    def cdf_func(df,mean,sd):
        cdf = []

        for index, row in df.iterrows():
            t = 1 - norm(loc = mean , scale = sd).cdf(row['Abs_diff'])
            cdf.append(t)

        return cdf

    def cdf_classifier(df,threshold):
        cls = []
        for index, row in df.iterrows():
            if row['cdf'] < threshold:
                cls.append(1)
            else:
                cls.append(0)

        return cls

    def window(seq, n=2):
        "Returns a sliding window (of width n) over data from the iterable"
        "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
        it = iter(seq)
        result = tuple(islice(it, n))
        if len(result) == n:
            yield result
        for elem in it:
            result = result[1:] + (elem,)
            yield result

    def deciding_classifier(df,window,error_threshold):
        win = Process.window(df['predicted_class'],window)
        faulty = 0
        li = []
        for i in win:
            err = (sum(i)/len(i))*100
            li.append(err)
            if  err >= error_threshold:
                faulty = 1

        return faulty,li

class Utils():

    def createFile(name, data):
        with open(name, 'w') as f:
            for item in data:
                f.write("%s\n" % item)

    def convertMonth(number):
        monthName = datetime.datetime.strptime(number, '%m')

        return monthName

        #open and read the file after the appending:
        # f = open("demofile3.txt", "r")
        # print(f.read())
