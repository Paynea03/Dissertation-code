#!/usr/bin/env python3

# import written classes
from process import Process
from process import Utils
from prog import CommandLine
from mlm import MachineLearningMastery as mlm
from scipy.stats import norm
from mlclassifier import MLClassifier

# import required packages
from array import *
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math, sys
import argparse
import seaborn as sb
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix, plot_confusion_matrix
from sklearn import svm
from itertools import islice
from sklearn.metrics import f1_score
from plot import Plots

import os
# disables tensorflow cpu performance printouts in terminal
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# lstm imports
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error


def detectAndAnalyse(args,fault_func,save_path,sensor_id,iteration,createdFile):
#
# # create a CSV with appropriate headers for 1 sensor
#     if iteration == 0:
#         createdFile = Process.buildDataSet(sensor_id,args.month,args.year,args.duration)

# read csv back to panda dataframe and create graph
    sensorReadingsData = pd.read_csv(createdFile)

# cast month and duration to string for further use in filenames
    month, duration = str(args.month), str(args.duration)
# create plot of raw data
    Plots.createActualPlot(month,args.year,sensorReadingsData,sensor_id,duration,save_path)

# add in erroneous data
    sensorReadingsData,type, faulty_class = fault_func(sensorReadingsData,args.training_split)

# remove unused columns
    sensorReadings = sensorReadingsData.drop(columns=['time','datetime','class'])

# pass to lstm model
    act, testPred, trainPred, trainLen, test_rmse = mlm.lstm(sensorReadings,
                        month,args.year,
                        sensor_id,
                        args.epoch_size,
                        args.batch_size,
                        duration,
                        args.training_split,
                        args.lstm_patience,
                        type,
                        save_path,
                        iteration)

# build into dataframes
    df = Process.modeled_dataframe(act,testPred, trainPred, trainLen,sensorReadingsData)

    x = df['Abs_diff'].tolist()

# compute means and Probability Density function
    pdf, mean, sd = Process.mean_sd_pdf(x)

# classify readings
    df['cdf'] = Process.cdf_func(df,mean,sd)

    # mlm.lstm_classifier(df)

    df['predicted_class'] = Process.cdf_classifier(df,args.cdf_threshold)

# build pdf curve and confusion matrix
    class_act = df['class']
    class_pred = df['predicted_class']

    print('fscore', f1_score(class_act, class_pred, average='macro'))

    Plots.create_confusion_matrix(class_act,class_pred,type,save_path)
    # Process.pdf_curve(x,pdf)

# decide if faulty or not
    pred_faulty, window_err = Process.deciding_classifier(df,30,args.error_threshold)
    print('Pred Faulty Sensor?:',pred_faulty)
    print('Actual Faulty Sensor?', faulty_class)

# write modeled dataframe to file
    # Process.createPredictedCSV(df,file_prepend,iteration)
    df.to_csv('csv/predicted/'+file_prepend+'-predicted-'+fault_func.__name__+'.csv',index=True)
#
    window_df = pd.DataFrame(window_err,columns=['%'])
    # print(vars(args))
    Plots.createProbabilityPlot(args,type,df['cdf'],save_path)

    Plots.createWindowPlot(type,window_df,args,pred_faulty,save_path)

    # classified_sensors.append([faulty_class,pred_faulty])
    return [faulty_class,pred_faulty]


if __name__ == '__main__':
    classified_sensors = []

    print('http://smartbms01.shef.ac.uk website data processing')

    computerRoom3TempSensors = ["BMS-L12O37S1","BMS-L12O37S2","BMS-L12O37S3","BMS-L12O37S6",
                                    "BMS-L11O30S1","BMS-L11O30S2","BMS-L11O30S6"]

    computerRoom3CO2Sensors = ["BMS-L11O30S7","BMS-L12O37S8"]

    computerRoom3AirConTempSensors = ["BMS-L11O32S3","BMS-L11O33S3","BMS-L11O34S3","BMS-L11O35S3",
                                        "BMS-L11O36S3","BMS-L11O37S3","BMS-L11O38S3","BMS-L11O39S3"]

    fault_types = [Process.no_fault,
                    Process.catastrophic_failure,
                    Process.drifting_failure,
                    Process.temporary_failure,
                    Process.erratic_failure]

# assign command line args to variables
    args = CommandLine.args()

    path = os.getcwd() + '/plots'
    month, duration = str(args.month), str(args.duration)

# download data and create csv
    createdFile = []

    for id in test:
        createdFile.append(Process.buildDataSet(id,args.month,args.year,args.duration))

    for file, id in createdFile:
        # create Sensor Dir
        file_prepend = str(id+'-'+Utils.convertMonth(month).strftime('%B')+args.year+'-'+duration+'months')
        Process.create_dir(path,file_prepend)
        new_sensor_path = path + '/' + file_prepend
        print('**********************Parsing Sensor: '+str(id)+'**********************')

        for i, type in enumerate(fault_types, start=0):
            # create nested dir
            Process.create_dir(new_sensor_path,type.__name__)
            new_type_path = new_sensor_path + '/' + type.__name__
            print('------------Parsing fault type: '+str(type.__name__)+'---------------------------')
            # process data
            result = detectAndAnalyse(args,type, new_type_path,id,i,file)
            classified_sensors.append(result)

    overall_class = pd.DataFrame.from_records(classified_sensors, columns=['Act_Class', 'Pred_Class'])
    print(overall_class)
    Plots.create_confusion_matrix(overall_class['Act_Class'],overall_class['Pred_Class'],'Total Sensors Classified',path)
